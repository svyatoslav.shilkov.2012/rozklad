function rozkladService({redis, config, axios, htmlParsingService}) {

    const initAutoUpdating = () => {
        refetchEverything()
        setInterval(refetchEverything, config.ROZKLAD_UPDATE_INTERVAL)
        // setInterval(getAllRozkladData, 20*60*1000)
    }

    const refetchEverything = async () => {
        const go = groupsLeft => {
            fetchAndCacheRozklad(groupsLeft[0])
            if (groupsLeft.length > 1) {
                setTimeout(() => go(groupsLeft.slice(1)), config.NEXT_GROUP_DELAY)
            } else {
                collectAndCacheAllData()
            }
        }
        go(await getGroups())
    }

    const globalSearch = async textEntry => {
        const data = await getAllRozkladData()
        const upperEntry = textEntry.toUpperCase()
        const found = []

        const notNullGroup = g => !!g[1]

        Object.entries(data).filter(notNullGroup).forEach(([group, {fetchedAt, institute, json}]) => {
            json.forEach(day => {
                day.pairs.forEach(pair => {
                    pair.details.forEach(details => {
                        Object.entries(details).forEach(([key, value]) => {
                            if (new String(value).toUpperCase().includes(upperEntry)) {
                                found.push({
                                    group: group,
                                    day: day.day,
                                    pair: pair.pairNum,
                                    field: key,
                                    details
                                })
                            }
                        })
                    })
                })
            })
        })

        return found
    }

    const getAllRozkladData = async () => {
        return JSON.parse(await redis.get(`rozklad-json-all`))
    }

    let timeout = null
    const triggerCollectAndCacheAll = () => {
        if (!timeout) {
            timeout = setTimeout(collectAndCacheAllData, 60 * 1000)
        }
    }

    const collectAndCacheAllData = async () => {
        timeout = null
        const collected = await collectAllData()
        await redis.set(`rozklad-json-all`, JSON.stringify(collected))
    }

    const collectAllData = async () => {
        const groups = await getGroups()
        const allGroups = await Promise.all(
            groups.map(async group =>
                ({[group]: JSON.parse(await redis.get(`rozklad-json:${group}`))})
            )
        )
        return allGroups.reduce((prev, cur) => ({...prev, ...cur}), {})
    }

    const getGroups = async () => {
        const fetchedRozklad = await fetchRozklad()
        if (fetchedRozklad) {
            const fetchedGroups = htmlParsingService.extractGroups(fetchedRozklad)
            await redis.sAdd('groups', fetchedGroups)
            return fetchedGroups
        } else {
            const cachedGroups = await redis.sMembers('groups')
            return cachedGroups || []
        }
    }

    const getRozkladData = async (group) => {
        const cached = JSON.parse(await redis.get(`rozklad-json:${group}`))
        if (!cached)
            return (await fetchAndCacheRozklad(group))?.json
        return cached
    }

    const getRozklad = async (group) => {
        const cached = await redis.get(`rozklad:${group}`)
        if (!cached)
            return (await fetchAndCacheRozklad(group))?.html
        return cached
    }

    const fetchAndCacheRozklad = async group => {
        const inst = await getInstitute(group)
        const fetched = await fetchRozklad(inst, group)
        if (fetched) {
            const json = htmlParsingService.extractRozkladData(fetched)
            const jsonWithTime = {fetchedAt: Date.now(), institute: inst, json}
            await cacheRozklad(group, fetched, jsonWithTime)
            triggerCollectAndCacheAll()
            return {json: jsonWithTime, html: fetched}
        } else return null
    }

    const cacheRozklad = async (group, rozklad, json) => {
        await redis.set(`rozklad:${group}`, rozklad, 'EX')
        await redis.set(`rozklad-json:${group}`, JSON.stringify(json))
    }

    const getInstitute = async group => {
        const cached = await redis.get(`institute:${group}`)
        if (!cached) {
            const fetched = await fetchInstitute(group)
            if (fetched)
                await redis.set(`institute:${group}`, fetched)
            return fetched
        }
        return cached
    }

    const fetchInstitute = async group => fetchRozklad(null, group)
        .then(htmlParsingService.extractInstitutes)
        .then(_ => _[0])

    const fetchRozklad = async (institute = 'All', group = 'All') => {
        const start = Date.now()
        return await rq(encodeURI(config.FETCH_URL(institute, group)))
            .catch(err => {
                console.log(`failed to fetch ${group}`)
                return undefined;
            })
            .finally(() => console.log(`fetch ${group} took ${Date.now() - start}`))
    }

    const rq = url => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        setTimeout(() => {
            source.cancel();
        }, config.REQUEST_TIMEOUT);
        return axios.get(url, {cancelToken: source.token}).then(req => req.data)
    }

    return {
        initAutoUpdating,
        globalSearch,
        getAllRozkladData,
        getRozkladData,
        getRozklad
    }
}

module.exports = rozkladService
