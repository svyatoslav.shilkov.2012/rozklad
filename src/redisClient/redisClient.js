const redisClient = require("redis");
const Config = require("../Config");

const client = redisClient.createClient({
    url: `redis://${Config.REDIS.HOST}:${Config.REDIS.PORT}`
});
client.connect()

client.on("error", function(error) {
    console.error(error);
});

module.exports = client
